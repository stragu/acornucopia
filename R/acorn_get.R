#' Download ACORN-SAT daily data
#'
#' Download ACORN-SAT daily temperature data and extract it into a directory.
#'
#' @param dir Destination directory where files will be extracted, as a string. (Defaults to "acorn_data".)
#' @param extreme Which temperature data to download, as a character string: "all", "min" or "max". (Defaults to "all".)
#'
#' @return
#' @export
#'
#' @examples
#' \dontrun{
#' acorn_get("path/to/directory", extreme = "min")
#' }
acorn_get <- function(dir = "acorn_data", extreme = "all") {
  if (!dir.exists(dir)) {
    dir.create(dir)
  }
  if (extreme %in% c("all", "max")) {
    download.file(url = "ftp://ftp.bom.gov.au/anon/home/ncc/www/change/ACORN_SAT_daily/acorn_sat_v2_daily_tmax.tar.gz",
                  destfile = "acorn_sat_v2_daily_tmax.tar.gz")
  }
  if (extreme %in% c("all", "min")) {
    download.file(url = "ftp://ftp.bom.gov.au/anon/home/ncc/www/change/ACORN_SAT_daily/acorn_sat_v2_daily_tmin.tar.gz",
                  destfile = "acorn_sat_v2_daily_tmin.tar.gz")
  }
  if (extreme %in% c("all", "max")) {
    untar(tarfile = "acorn_sat_v2_daily_tmax.tar.gz",
          exdir = dir)
  }
  if (extreme %in% c("all", "min")) {
    untar(tarfile = "acorn_sat_v2_daily_tmin.tar.gz",
          exdir = dir)
  }
}
