Contributions to this repository are more than welcome! They are expecially welcome when communication is done in an inclusive and friendly manner that assumes people are well-meaning.

We particularly welcome contributions from people who have a diverse background, as the recent historical default makeup of the scientific computing community (i.e. overwhelmingly white cishet male) is a self-perpetuating problem that actively needs to be challenged.

We do not welcome any interaction that the maintainer(s) consider to be oppressive, disrespectful or violent, or qualify as "punching down".

In particular, we will not tolerate:

* racism,
* misoginy,
* transphobia,
* deliberate misgendering,
* homophobia,
* ableism,
* and fatphobia.
