#' Merge ACORN-SAT stations' data
#'
#' This function reads, cleans and merges all
#' the ACORN-SAT temperature CSV files found in a directory into
#' a single tibble.
#'
#' @param dir Path to directory containing the CSV files (as a string). Defaults to "acorn_data".
#' @param with_station_info Should the station metadata be added to the tibble (logical). Defaults to FALSE.
#'
#' @return A clean tibble
#' @export
#' @importFrom purrr map_dfr
#' @importFrom dplyr full_join left_join
#' @examples
#' \dontrun{
#' acorn_merge("path/to/directory")
#' }
acorn_merge <- function(dir = "acorn_data", with_station_info = FALSE) {
  if (!dir.exists(dir)) {
    stop("The directory does not exist. Please provide a valid path as a string.")
  }
  files <- list.files(path = dir,
                      pattern = "csv",
                      full.names = TRUE)
  if (any(grepl("tmax", files)) & any(grepl("tmin", files))) {
    temp_max <- map_dfr(files[grepl("tmax", files)], acorn_read)
    temp_min <- map_dfr(files[grepl("tmin", files)], acorn_read)
    df <- full_join(temp_max, temp_min)
  } else {
    df <- map_dfr(files, acorn_read)
  }
  if (with_station_info) {
    df <- left_join(df, acorn_read_info(dir))
  }
  df
}
